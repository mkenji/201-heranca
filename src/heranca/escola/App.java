package heranca.escola;

public class App {
	public static void main(String[] args) {
		
		//criando os alunos
		Aluno[] alunos = new Aluno[2];
		Aluno aluno1 = new Aluno();
		Aluno aluno2 = new Aluno();
		aluno1.setNome("Joazinho");
		aluno2.setNome("Mariazinha");
		alunos[0] = aluno1;
		alunos[1] = aluno2;
				
		//criando o professor
		Professor professor1 = new Professor();
		professor1.setNome("Joao");
		
		//Criando o administrador
		Administrador admin = new Administrador();
		admin.setNome("Mestre");
		
		//Criando a turma
		Turma turma01 = new Turma();
		turma01.setAnoLetivo(2018);
		turma01.setLetra('A');
		turma01.setAlunos(alunos);
		
		//Criando a materia
		Materia materia1 = new Materia();
		materia1.setNome("Matematica");
		materia1.setTurma(turma01);
		
		System.out.println(materia1.getTurma().getAnoLetivo());
		
	}
}
