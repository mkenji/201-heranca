package heranca.veiculos;

public class Veiculo {
	protected int ano;
	protected String modelo;
	protected String placa;
	protected String fabricante;
	protected int id;
	
	public int getAno() {
		return ano;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getFabricante() {
		return fabricante;
	}
	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "Veiculo [ano=" + ano + ", modelo=" + modelo + ", placa=" + placa + ", fabricante=" + fabricante
				+ ", id=" + id + "]";
	}
	
}
