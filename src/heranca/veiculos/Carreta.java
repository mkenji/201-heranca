package heranca.veiculos;

public class Carreta extends Veiculo{
	private String comprimento;
	private Tipo tipo;
	
	public String getComprimento() {
		return comprimento;
	}
	public void setComprimento(String comprimento) {
		this.comprimento = comprimento;
	}
	public Tipo getTipo() {
		return tipo;
	}
	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}
	@Override
	public String toString() {
		return "Carreta [comprimento=" + comprimento + ", tipo=" + tipo + "]";
	}



}
