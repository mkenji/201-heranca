package heranca.veiculos;

public enum Tipo {
	Graos("Grãos"),
	Liquidos("Liquidos"),
	Bau("Baú");

	private final String nome;

	Tipo(String nome) {
		this.nome = nome;
	}

	public String toString() {
		return this.nome;
	}

}
