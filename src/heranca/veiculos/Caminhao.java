package heranca.veiculos;

public class Caminhao extends Veiculo{
	private String cor;
	private Carreta[] carretas;

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public Carreta[] getCarretas() {
		return carretas;
	}

	public void setCarretas(Carreta[] carretas) {
		this.carretas = carretas;
	}

}
