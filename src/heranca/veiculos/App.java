package heranca.veiculos;

public class App {
	public static void main(String[] args) {
		
		Carro carro = new Carro();
		carro.setAno(2018);
		carro.setCor("Azul");
		carro.setFabricante("Honda");
		carro.setId(1);
		carro.setModelo("Fit");
		carro.setPlaca("ABC-1234");
		//System.out.println(carro.getFabricante());
	
		Carreta[] carretas = new Carreta[2];
		
		carretas[0] = new Carreta();
		carretas[1] = new Carreta();
		carretas[0].setFabricante("Teste");
		carretas[0].setTipo(Tipo.Bau);
		carretas[1].setFabricante("Fabricante2");
		carretas[1].setTipo(Tipo.Liquidos);
			
		Caminhao caminhao = new Caminhao();
		caminhao.setCarretas(carretas);
		
		for(Carreta carretaItens: carretas) {
			//System.out.println(carretaItens.getFabricante());
			System.out.println(carretaItens);
		}
		
		
	}
}
