package heranca;

import heranca.hospital.Medico;
import heranca.hospital.Paciente;
import heranca.hospital.Pessoa;

public class App {
	public static void main(String[] args) {
		Pessoa[] pessoas = new Pessoa[2];
		
		Medico medico = new Medico();
		Pessoa paciente = new Paciente();
		
		//Tanto um médico como um paciente 
		//podema atuar como a classe Pessoa
		pessoas[0] = medico;
		pessoas[1] = paciente;
		
		for(Pessoa pessoa: pessoas) {
			//Veja que o tipo da classe não muda
			System.out.println(pessoa.getClass());
		}
		
		medico.setNome("Joao");
		medico.setCrm("112312312");
		
		paciente.setNome("Jose");
		paciente.setSexo('M');
		
		System.out.println(medico.getCrm());
	}
}
