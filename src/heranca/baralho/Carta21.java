package heranca.baralho;

public class Carta21 extends Carta{
	
	private int valor;
	
	public Carta21(String rank, String naipe, int valor) {
		super(rank, naipe);
		this.valor = valor;
	}
	
	public int getValor() {
		return valor;
	}
	
	
}
